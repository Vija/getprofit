



def get_max_profit(stock_prices_yesterday):

    minimum = stock_prices_yesterday[0]
    result = 0
    max_profit = []

    for price in stock_prices_yesterday:
        minimum = min(minimum, price)
        result = max(result, price-minimum)
        max_profit.append(result)

    return max(max_profit)


